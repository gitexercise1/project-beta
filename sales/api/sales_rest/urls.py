from django.urls import path
from .views import (
    api_salesperson,
    api_list_customer,
    api_list_sales,
    api_salesperson_details,
    api_customer_details,
    api_sales_details
)

urlpatterns = [
    path('salespeople/', api_salesperson, name="api_salesperson"),
    path('customers/', api_list_customer, name="api_list_customer"),
    path('sales/', api_list_sales, name="api_list_sales"),
    path('salespeople/<int:id>/', api_salesperson_details, name="api_salesperson_details"),
    path('customers/<int:id>/', api_customer_details, name='api_customer_details'),
    path('sales/<int:id>/', api_sales_details, name='api_sales_details'),
]
