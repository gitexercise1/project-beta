import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './servicecomponents/technicianForm';
import TechnicianList from './servicecomponents/technicianList';
import AppointmentForm from './servicecomponents/appointmentform';
import AppointmentList from './servicecomponents/appointmentList';
import ServiceHistory from './servicecomponents/appointmenthistory';
import ManufacturerForm from './inventorycomponents/manufacturersForm';
import ManufacturersList from './inventorycomponents/manufacturersList';
import VehicleModelForm from './inventorycomponents/modelsForm';
import ModelList from './inventorycomponents/modelsList';
import AutomobileForm from './inventorycomponents/automobilesForm';
import AutomobileList from './inventorycomponents/automobilesList';

// import TechnicianForm from './servicecomponents/technicianForm';
// import TechnicianList from './servicecomponents/technicianList';
// import AppointmentForm from './servicecomponents/appointmentform';
// import AppointmentList from './servicecomponents/appointmentList';
// import ServiceHistory from './servicecomponents/appointmenthistory';
// import ManufacturerForm from './inventorycomponents/manufacturersForm';
// import ManufacturersList from './inventorycomponents/manufacturersList';
// import VehicleModelForm from './inventorycomponents/modelsForm';
// import ModelList from './inventorycomponents/modelsList';
// import AutomobileForm from './inventorycomponents/automobilesForm';
// import AutomobileList from './inventorycomponents/automobilesList';
import SalesPersonList from './SalesPersonList';
import SalespersonForm from './SalesPersonForm';
import CustomerList from './customerlist';
import CustomerForm from './customerform';
import SaleList from './Salelist';
import SaleForm from './Saleform';
import SalesPersonHistory from './SalesPersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/new/" element={<TechnicianForm />} />
          <Route path="/technicians/" element={<TechnicianList />} />
          <Route path="/appointments/new/" element={<AppointmentForm />} />
          <Route path="/appointments/" element={<AppointmentList />} />
          <Route path="/servicehistory/" element={<ServiceHistory />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/manufacturers/" element={<ManufacturersList />} />
          <Route path="/models/new/" element={<VehicleModelForm />} />
          <Route path="/models/" element={<ModelList />} />
          <Route path="/auto/new/" element={<AutomobileForm />} />
          <Route path="/auto/" element={<AutomobileList />} />




          <Route path="salespeople">
            <Route index element={<SalesPersonList />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>
          <Route path="customer">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sale">
            <Route index element={<SaleList />} />
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SalesPersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
