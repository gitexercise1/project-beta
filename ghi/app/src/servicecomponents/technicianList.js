import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

const deleteTechnician = async (id) => {
    const fetchConfig = {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        },
    }
    await fetch(`http://localhost:8080/api/technicians/${id}/`, fetchConfig);
    window.location.reload()
}

function TechnicianList() {
    const [technicians, setTechnicians] = useState([])

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const technicianUrl = "http://localhost:8080/api/technicians/"

        const technicianResponse = await fetch(technicianUrl);
        if (technicianResponse.ok) {
            const data = await technicianResponse.json()
            setTechnicians(data.technicians)
        }
    }

    return (
        <div>
            <Link to="/technicians/new/" className="btn btn-success m-2 w-100">
                Add Technician
            </Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th> Employee ID </th>
                        <th> Delete </th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td> {technician.first_name} </td>
                                <td> {technician.last_name} </td>
                                <td> {technician.employee_id} </td>
                                <td>
                                    <button type="button" onClick={() => deleteTechnician(technician.id)} className='btn btn-danger'>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default TechnicianList;
