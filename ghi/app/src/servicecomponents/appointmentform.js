import React, { useState, useEffect } from 'react';

function AppointmentForm() {
  const [state, setState] = useState({
    vin: "",
    customer: "",
    date: "",
    technician: "",
    reason: "",
  });
  const [technicians, setTechnicians] = useState([]);
  const [successfulSubmit, setSuccessfulSubmit] = useState(false);

  useEffect(() => {
    const fetchTechnicians = async () => {
      const response = await fetch('http://localhost:8080/api/technicians/');
      const data = await response.json();
      setTechnicians(data.technicians);
    };

    fetchTechnicians();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    const data = {
      vin: state.vin,
      customer: state.customer,
      date_time: state.date,
      technician_id: state.technician,
      reason: state.reason,
    };

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);

    if (response.ok) {
      setState({
        vin: "",
        customer: "",
        date: "",
        technician: "",
        reason: "",
      });
      setSuccessfulSubmit(true);
    }
  };

  const handleChange = event => {
    const value = event.target.value;
    setState({
      ...state,
      [event.target.name]: value,
    });
  };

  return (
    <div className='container'>
      <h1>Create a new appointment</h1>
      <form onSubmit={handleSubmit} id='create-appointment-form'>
        <div className='mb-3'>
          <input onChange={handleChange} value={state.vin} placeholder='VIN' required name='vin' id='vin' className='form-control' />
        </div>
        <div className='mb-3'>
          <input onChange={handleChange} value={state.customer} placeholder='Customer' required name='customer' id='customer' className='form-control' />
        </div>
        <div className='mb-3'>
          <input onChange={handleChange} value={state.date} placeholder='Date' required name='date' type='datetime-local' id='date' className='form-control' />
        </div>
        <div className='mb-3'>
          <label htmlFor='technician'>Technician</label>
          <select
            onChange={handleChange}
            value={state.technician}
            name='technician'
            id='technician'
            className='form-control'
            required
          >
            <option value="">Select a technician</option>
            {technicians.map(tech => (
              <option key={tech.id} value={tech.id}>
                {tech.first_name} {tech.last_name} (ID: {tech.employee_id})
              </option>
            ))}
          </select>
        </div>
        <div className='mb-3'>
          <input onChange={handleChange} value={state.reason} placeholder='Reason' required name='reason' id='reason' className='form-control' />
        </div>
        <button className='btn btn-primary'>Create</button>
      </form>
      {successfulSubmit && (
        <div className='alert alert-success mt-3'>
          Appointment created successfully
        </div>
      )}
    </div>
  );
}

export default AppointmentForm;
