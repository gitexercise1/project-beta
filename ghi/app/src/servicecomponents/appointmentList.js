import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [inventory, setInventory] = useState([]);

  useEffect(() => {
    fetchAppointments();
    fetchInventory();
  }, []);



  const fetchAppointments = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };




  const fetchInventory = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const data = await response.json();
      setInventory(data.automobiles);
    }
  };

  const updateAppointmentStatus = async (id, status) => {
    const url = `http://localhost:8080/api/appointments/${id}/${status}/`;
    await fetch(url, { method: 'PUT' });
    fetchAppointments();
  };





  return (
    <div className='container'>
      <h1>Service Appointments</h1>
      <Link to="/appointments/new" className="btn btn-primary mb-3">Add New Appointment</Link>
      <h3>VIP: if vin number is the same as our inventory record and the auto is sold.</h3>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date & Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleString()}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
              <td>
                <button onClick={() => updateAppointmentStatus(appointment.id, 'cancel')} className='btn btn-danger me-2'>Cancel</button>
                <button onClick={() => updateAppointmentStatus(appointment.id, 'finish')} className='btn btn-success'>Finish</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );


          }

          export default AppointmentList;
