import React, { useState } from "react";

function TechnicianForm() {
  const [state, setState] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });
  const [successfulSubmit, setSuccessfulSubmit] = useState(false);

  let formClasses = "";
  let alertClasses = "alert alert-success d-none mb-3";
  let alertContainerClasses = "d-none";

  const handleSubmit = async event => {
    event.preventDefault();
    const data = {
      first_name: state.first_name,
      last_name: state.last_name,
      employee_id: state.employee_id,
    };

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);

    if (response.ok) {
      setState({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
      setSuccessfulSubmit(true);
    }
  };

  const handleChange = event => {
    const value = event.target.value;
    setState({
      ...state,
      [event.target.name]: value,
    });
  };

  if (successfulSubmit) {
    formClasses = "d-none";
    alertClasses = "alert alert-success mb-3";
    alertContainerClasses = "";
  }

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new technician</h1>
          <form onSubmit={handleSubmit} id='create-technician-form' className={formClasses}>
            <div className='form-floating mb-3'>
              <input onChange={handleChange} value={state.first_name} placeholder='First Name' required name='first_name' id='first_name' className='form-control' />
              <label htmlFor='first_name'>First Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input onChange={handleChange} value={state.last_name} placeholder='Last Name' required name='last_name' id='last_name' className='form-control' />
              <label htmlFor='last_name'>Last Name</label>
            </div>
            <div className='form-floating mb-3'>
              <input onChange={handleChange} value={state.employee_id} placeholder='Employee ID' required name='employee_id' id='employee_id' className='form-control' />
              <label htmlFor='employee_id'>Employee ID</label>
            </div>
            <button className='btn btn-primary'>Create</button>
          </form>
          <div className={alertContainerClasses}>
            <div className={alertClasses} id='success-message'>
              Great!New Technician created successfully
            </div>
            <button onClick={() => setSuccessfulSubmit(false)} className='btn btn-primary'>
              Create another Technician
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
