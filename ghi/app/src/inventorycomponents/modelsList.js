import React, { useState, useEffect } from 'react';

function ModelList() {
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {
    const fetchVehicleModels = async () => {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setVehicleModels(data.models);
      }
    };

    fetchVehicleModels();
  }, []);

  return (
    <div className='container'>
      <h1>Vehicle Models</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Picture</th>
            <th>Manufacturer Name</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((model) => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>
                {model.picture_url && <img src={model.picture_url} alt={model.name} style={{ maxWidth: '100px' }} />}
              </td>

              <td>{model.manufacturer && model.manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ModelList;
