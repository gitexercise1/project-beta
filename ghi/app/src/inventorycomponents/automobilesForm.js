import React, { useState, useEffect } from 'react';

function AutomobileForm() {
  const [newAuto, setNewAuto] = useState({
    color: '',
    year: '',
    vin: '',
    sold: false,
    model_id: ''
  });
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {

    const fetchVehicleModels = async () => {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setVehicleModels(data.models);
      }
    };

    fetchVehicleModels();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    await fetch('http://localhost:8100/api/automobiles/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newAuto),
    });

    setNewAuto({
      color: '',
      year: '',
      vin: '',
      sold: false,
      model_id: ''
    });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewAuto({ ...newAuto, [name]: name === 'sold' ? event.target.checked : value });
  };

  return (
    <div className='container'>
      <h1>Add Automobile</h1>
      <form onSubmit={handleSubmit} className='shadow p-4'>
        <div className='form-group'>
          <label htmlFor='color'>Color:</label>
          <input type='text' id='color' name='color' className='form-control' value={newAuto.color} onChange={handleChange} required />
        </div>
        <div className='form-group'>
          <label htmlFor='year'>Year:</label>
          <input type='number' id='year' name='year' className='form-control' value={newAuto.year} onChange={handleChange} required />
        </div>
        <div className='form-group'>
          <label htmlFor='vin'>VIN:</label>
          <input type='text' id='vin' name='vin' className='form-control' value={newAuto.vin} onChange={handleChange} required />
        </div>
        <div className='form-group'>
          <label htmlFor='sold'>Sold:</label>
          <input type='checkbox' id='sold' name='sold' className='form-check-input' checked={newAuto.sold} onChange={handleChange} />
        </div>
        <div className='form-group'>
          <label htmlFor='model_id'>Model:</label>
          <select id='model_id' name='model_id' className='form-control' value={newAuto.model_id} onChange={handleChange} required>
            <option value=''>Select a Model</option>
            {vehicleModels.map((model) => (
              <option key={model.id} value={model.id}>
                {model.name}
              </option>
            ))}
          </select>
        </div>
        <button type='submit' className='btn btn-primary'>Submit</button>
      </form>
    </div>
  );
}

export default AutomobileForm;
